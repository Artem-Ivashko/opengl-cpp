#include <string>
#include <map>

using namespace std;

#include <glad/glad.h>


class Shader
{
public:
  GLuint ID;
  // constructor generates the shader on the fly
  // ------------------------------------------------------------------------
  Shader(const string vertexPath, const string fragmentPath);
};

// utility function for checking shader compilation/linking errors.
void checkCompileErrors(const GLuint shader, const string type);
// activate the shader
void useShader(const GLuint shaderID);

void setUniforms(const GLuint ID, const string uniformName,
                 const map<string, GLfloat> uniformMap);
