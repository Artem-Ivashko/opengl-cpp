#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cstring>

#include <glad/glad.h>

#include "shader_class.hpp"

Shader::Shader(const string vertexPath, const string fragmentPath)
{
  // 1. retrieve the vertex/fragment source code from filePath
  string vertexCode;
  string fragmentCode;
  ifstream vShaderFile;
  ifstream fShaderFile;
  ifstream gShaderFile;
  // ensure ifstream objects can throw exceptions:
  vShaderFile.exceptions (ifstream::failbit | ifstream::badbit);
  fShaderFile.exceptions (ifstream::failbit | ifstream::badbit);
  gShaderFile.exceptions (ifstream::failbit | ifstream::badbit);
  try {
    // open files
    vShaderFile.open(vertexPath);
    fShaderFile.open(fragmentPath);
    stringstream vShaderStream, fShaderStream;
    // read file's buffer contents into streams
    vShaderStream << vShaderFile.rdbuf();
    fShaderStream << fShaderFile.rdbuf();		
    // close file handlers
    vShaderFile.close();
    fShaderFile.close();
    // convert stream into string
    vertexCode = vShaderStream.str();
    fragmentCode = fShaderStream.str();			
  }
  catch (ifstream::failure& e) {
    cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << endl;
  }
  const char* vShaderCode = vertexCode.c_str();
  const char * fShaderCode = fragmentCode.c_str();
  // 2. compile shaders
  unsigned int vertex, fragment;
  // vertex shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, &vShaderCode, NULL);
  glCompileShader(vertex);
  checkCompileErrors(vertex, "VERTEX");
  // fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, &fShaderCode, NULL);
  glCompileShader(fragment);
  checkCompileErrors(fragment, "FRAGMENT");
  // shader Program
  ID = glCreateProgram();
  glAttachShader(ID, vertex);
  glAttachShader(ID, fragment);
  glLinkProgram(ID);
  checkCompileErrors(ID, "PROGRAM");
  // delete the shaders as they're linked into our program now and no longer necessery
  glDeleteShader(vertex);
  glDeleteShader(fragment);
}


void useShader(const GLuint shaderID) {
  glUseProgram(shaderID);
  map<string, GLfloat> uniformMap;
  uniformMap.insert({"speed", 0.1f});
  uniformMap.insert({"lineWidth", 0.05f});
  uniformMap.insert({"angle", -30.0f});
  uniformMap.insert({"spacing", 0.1f});
  uniformMap.insert({"visibility", 0.5f});
  setUniforms(shaderID, "Uniforms", uniformMap);
}


void checkCompileErrors(const GLuint shaderID, const string type)
{
  GLint success;
  GLchar infoLog[1024];
  if(type != "PROGRAM") {
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
    if(!success) {
      glGetShaderInfoLog(shaderID, 1024, NULL, infoLog);
      cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << endl;
    }
  }
  else {
    glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
    if(!success) {
      glGetProgramInfoLog(shaderID, 1024, NULL, infoLog);
      cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << endl;
    }
  }
};


void setUniforms(const GLuint ID, const string uniformName, const map<string, GLfloat> uniformMap) {
  GLuint index = glGetUniformBlockIndex(ID, uniformName.c_str());
  GLint bufferSize;
  glGetActiveUniformBlockiv(ID, index,
                            GL_UNIFORM_BLOCK_DATA_SIZE, &bufferSize);

  int nameSize = uniformMap.size();
  char** names = (char**)malloc(nameSize * sizeof(char*));
  GLfloat* values = (GLfloat*)malloc(nameSize * sizeof(GLfloat));
  int i=0;
  for(auto const& key_value: uniformMap) {
    string name = key_value.first;
    names[i] = (char*)malloc(name.size());
    strcpy(names[i], name.c_str());
    values[i] = key_value.second;
    i++;
  }

  GLuint indices[nameSize];
  glGetUniformIndices(ID, nameSize, names, indices);
  for(int i=0; i < nameSize; i++) {
    free(names[i]);
    // cout << indices[i] << endl;
  }
  free(names);

  GLint size[nameSize];
  GLint offset[nameSize];
  glGetActiveUniformsiv(ID, nameSize, indices,
                        GL_UNIFORM_OFFSET, offset);
  glGetActiveUniformsiv(ID, nameSize, indices,
                        GL_UNIFORM_SIZE, size);

  GLvoid* buffer = malloc(bufferSize);
  for (int i=0; i<nameSize; i++)
    memcpy(buffer + offset[i], &values[i],
           size[i] * sizeof(GLfloat));
  free(values);

  // Create the uniform buffer object
  GLuint bufferObject;
  glGenBuffers(1, &bufferObject);
  glBindBuffer(GL_UNIFORM_BUFFER, bufferObject);
  glBufferData(GL_UNIFORM_BUFFER, bufferSize,
               buffer, GL_STATIC_DRAW);
  glBindBufferBase(GL_UNIFORM_BUFFER, index, bufferObject);
  free(buffer);
}
