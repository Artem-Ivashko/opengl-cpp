INCLUDE_FLAGS = -I./glad/include -I.
LIBRARY_FLAGS = -ldl -lglfw

# In order to install on Ubuntu libglfw together with headers like GLFW/glfw3.h
install-glfw3:
	sudo apt install libglfw3-dev

# In order to generate the glad.c file
setup-glad:
	pip3 install glad
	~/.local/bin/glad --spec gl --profile core \
		--api gl=3.3 \
		--out-path=./glad \
		--generator c

clean-glad:
	rm -rf glad/

build-shader:
	g++ ${INCLUDE_FLAGS} glfw-glad.cpp gl-code.cpp shader_class.cpp glad/src/glad.c \
		-o hello-shader ${LIBRARY_FLAGS}

run-shader: build-shader
	./hello-shader

clean-compiled:
	rm hello-shader
