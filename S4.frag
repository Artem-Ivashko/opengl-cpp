#version 330 core

const float pi = 3.14159265358979323846;

uniform Uniforms {
 float speed;
 float lineWidth;
 float angle;
 float spacing;
 float visibility;
};

in vec4 gl_FragCoord;
out vec4 FragColor;

// patern function to create the lines
float pattern() {
  float s = sin((angle*pi/180));
  float c = cos((angle*pi/180));
  vec2 tex = gl_FragCoord.xy;
  vec2 point = vec2( c * tex.y - s * tex.x, s * tex.y + c * tex.x ) * 0.002;
  float d = (s * tex.y + c * tex.x )*0.002;
  return (mod((d + lineWidth + speed ),spacing*2));
}

void main() {
  vec4 out_color = vec4(0.0, 0.0, 0.0, 1.0);
  if ((pattern() > 0.0)&&(pattern() <= lineWidth)) {
  // filling in to use hard lines
  float percent = (2.0-abs(lineWidth-1.0*pattern())/lineWidth);
  percent = clamp(percent,0.0,1.0);
  out_color = mix(out_color,
  vec4(1.0-out_color.r, 1.0-out_color.g, 1.0-out_color.b, out_color.a),
  percent * visibility);
  }
  FragColor = out_color;
}