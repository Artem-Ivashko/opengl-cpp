#include <glad/glad.h>

#include "shader_class.hpp"

static GLuint VBO, VAO;
static GLuint shaderID;

void setup_opengl() {
  // ------------------------------------------------------------------
  const GLfloat vertices[] = {
    // positions
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    1.0f, 1.0f, 0.0f
  };

  glGenVertexArrays(1, &VAO);
  // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
  glBindVertexArray(VAO);

  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
}


void render_opengl() {
  glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  // Shaders are hot-reloaded on every frame rendering
  // (Good for development, better to disable for the production)
  Shader myShader("S4.vert", "S4.frag");
  shaderID = myShader.ID;
  useShader(shaderID);

  glBindVertexArray(VAO);
  // render the triangle
  // GL_TRIANGLE_STRIP tells to share vertices of subsequent triangles
  // 4 is the number of distinct triangle vertices
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


// optional: de-allocate all resources once they've outlived their purpose:
void cleanup_opengl() {
  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
}
